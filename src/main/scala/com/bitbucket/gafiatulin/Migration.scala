package com.bitbucket.gafiatulin

import cats.syntax.all._
import cats.effect.{Concurrent, Sync, Timer}
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.configuration.FluentConfiguration
import org.flywaydb.core.api.logging.{Log, LogFactory}

import scala.concurrent.duration.FiniteDuration

/**
  * Created by victor on 10/05/2017.
  */


case object Migration{
  private final val log: Log = LogFactory.getLog(Migration.getClass)
  private final def setDataSourceWithUrl(configuration: FluentConfiguration, config: DBConfig)(url: String): FluentConfiguration =
    configuration.dataSource(url, config.user, config.password)
  private final def timeout[F[_]: Concurrent : Timer, T](
    f: F[T],
    duration: Option[FiniteDuration],
    fallback: F[T]
  ): F[T] = duration.fold(f){duration =>
    Concurrent[F].race(f, Timer[F].sleep(duration)).flatMap{
      case Left(a) => a.pure
      case Right(_) => fallback
    }
  }

  private final def createIfNotExist[F[_]: Sync](
    configuration: FluentConfiguration,
    config: MigrationConfig
  ): F[Unit] = if(!config.createIfNotExist) Sync[F].unit else Sync[F].bracket{
    Sync[F].delay{
      setDataSourceWithUrl(configuration, config.db)(config.db.url.take(config.db.url.indexOfSlice(config.db.name)) + config.dbSpecificDefaultDb.getOrElse(""))
        .getDataSource
        .getConnection
    }
  }{connection =>
    Sync[F].delay{
      val existenceCheck = connection.createStatement()
      log.info(s"Running Database existence check: ${config.dbSpecificExistenceCheckQuery.getOrElse("")}")
      val rs = existenceCheck.executeQuery(config.dbSpecificExistenceCheckQuery.getOrElse(""))
      if (!rs.next()) {
        log.info(s"Database existence check failed. Creating table: ${config.dbSpecificDatabaseCreationQuery.getOrElse("")}")
        val dbCreation = connection.createStatement()
        dbCreation.execute(config.dbSpecificDatabaseCreationQuery.getOrElse(""))
        dbCreation.close()
        log.info("Created Database " + config.db.name)
        ()
      } else {
        log.debug(s"Database ${config.db.name} already exists")
        ()
      }
    }
  }{connection =>
    Sync[F].delay(connection.close())
  }

  private final def runF[F[_]: Concurrent : Timer, T](config: MigrationConfig)(f: FluentConfiguration => F[T]): F[Unit] =
    Concurrent[F].delay(Flyway.configure()).flatMap{configuration =>
      timeout[F, T](
        createIfNotExist[F](configuration, config).flatMap{_ =>
          f(setDataSourceWithUrl(configuration, config.db)(config.db.url))
        },
        config.migrationTimeout,
        Concurrent[F].raiseError[T](new Exception(s"Migration timed out ${config.migrationTimeout.fold("") { d => "after " + d.toString() }}"))
      )
    }.map(_ => ())

  final def migrate[F[_]: Concurrent : Timer](config: MigrationConfig): F[Unit] =
    runF(config)(configuration => Concurrent[F].delay{
      configuration.baselineOnMigrate(config.baseline).load().migrate()
      ()
    })

  final def reloadScheme[F[_]: Concurrent : Timer](config: MigrationConfig): F[Unit] =
    runF(config)(configuration => Concurrent[F].delay{
      val flyway = configuration.load()
      flyway.clean()
      flyway.migrate()
      ()
    })
}