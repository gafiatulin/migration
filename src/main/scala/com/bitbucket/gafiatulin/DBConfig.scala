package com.bitbucket.gafiatulin

/**
  * Created by victor on 10/05/2017.
  */

case class DBConfig(url: String, user: String, password: String, name: String)

case object DBConfig{
  def fromConfig(c: com.typesafe.config.Config): DBConfig =
    DBConfig(c.getString("url"), c.getString("user"), c.getString("password"), c.getString("name"))
}