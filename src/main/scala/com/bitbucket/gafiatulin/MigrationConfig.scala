package com.bitbucket.gafiatulin

import scala.concurrent.duration.FiniteDuration
import scala.util.Try

/**
  * Created by victor on 10/05/2017.
  */

case class MigrationConfig(
  db: DBConfig,
  baseline: Boolean,
  migrationTimeout: Option[FiniteDuration],
  createIfNotExist: Boolean,
  dbSpecificDefaultDb: Option[String],
  dbSpecificExistenceCheckQuery: Option[String],
  dbSpecificDatabaseCreationQuery: Option[String]
){
  require(
    if(createIfNotExist) dbSpecificDefaultDb.isDefined && dbSpecificExistenceCheckQuery.isDefined && dbSpecificDatabaseCreationQuery.isDefined else true,
    "Missing required configuration for database creation"
  )
}

case object MigrationConfig{
  def fromConfig(c: com.typesafe.config.Config): MigrationConfig =
    MigrationConfig(
      DBConfig.fromConfig(c.getConfig("db")),
      Try(c.getBoolean("baseline")).getOrElse(true),
      Try(c.getDuration("migration-timeout")).toOption.map(d => scala.concurrent.duration.Duration.fromNanos(d.toNanos)),
      Try(c.getBoolean("create-if-not-exist")).getOrElse(true),
      Try(c.getString("db-specific-default-db")).toOption,
      Try(c.getString("db-specific-existence-check-query")).toOption,
      Try(c.getString("db-specific-database-creation-query")).toOption
    )
}